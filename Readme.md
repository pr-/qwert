Education status
 - Bachelor of Computing

Employment status
 - Unemployed by 2018-11-11.

Location status
 - Ulaanbaatar, Mongolia

Marital status
 - Single

Experience as sales manager /2 years/

 - Presentations, proposals, negiotiations, logistics, profit estimations, cash management.

Experience as developer /3 years/

 - Targeted advertisement platform. /~1 year/   
Ruby on Rails, AWS (Linux, EC2, ELB, S3, Cloudfront, RDS), Adsense   

 - Financial software system improvements. /~1 year/   
.Net C++, C# windows forms, ASP web forms, Oracle DB.   

 - SAAS SPA prototype for a start up. (~3 months)   
NodeJS, Express, MongoDB, Backbone / Angular, Heroku   

 - Proof of concept web application for digital marketing. (~3 months)   
Java, Spring, MySQL, Serverless, AWS (EC2, Lambda, API Gateway)   

 - Small projects /~2 months/   
Web Scrapping, Scripting, Screen Saver, Email campaign tool, Bug fix     

 - Legacy HR SPA clean up, feature updates. /~2 months/   
PHP, CodeIgniter, MySQL, Angular   

 - Diabetes control web customization. /~1 month/   
NodeJS, Express, MySQL, Azure   

 - Router monitoring web and shipment handling web updates /~1 month/   
PHP, Custom PHP / CodeIgnitor, MySQL   

